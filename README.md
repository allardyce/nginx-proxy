# Automated Nginx Proxy

1. Install [Docker](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04) + [Docker Compose](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04)

2. Create main network `docker network create main`

3. Start nginx-proxy `docker-compose up -d`

4. Start web containers on the `main` network

    ```yaml
    # docker-compose.yml

    version: '3'

    services:
      foo.bar:
        image: nginx:alpine
        container_name: foo.bar
        restart: always
        environment:
          - VIRTUAL_HOST=foo.bar
          - LETSENCRYPT_HOST=foo.bar
          - LETSENCRYPT_EMAIL=baz@foo.bar

    networks:
      default:
        external:
          name: main

    ```